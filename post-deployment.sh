#!/bin/bash -xe

juju run -a jenkins -- systemctl enable docker
juju run -a jenkins -- systemctl restart docker
juju run -a jenkins -- usermod -aG docker ubuntu
juju run -a jenkins -- newgrp docker
juju run -a jenkins -- docker run hello-world
